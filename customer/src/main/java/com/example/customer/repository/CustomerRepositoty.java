package com.example.customer.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.customer.model.Customer;

public interface CustomerRepositoty extends CrudRepository<Customer, Long>{

}
