package com.example.customer.util;

public class IdGen {
	
	private IdGen() {}
	public static Long getId() {
		return (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
	}
}
