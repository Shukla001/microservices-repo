# Setup

**Pre-requite:** Postgres DB is required.

clone the repository: 
```
https://gitlab.com/Shukla001/microservices-repo.git
```

# Local setup

**Make sure Postgres is up and running. and update Postgres user name password in `application.properties` file if required**

Go to the project directory and run following command to build the jar and run the application

`mvn clean install`

**Should see DB query in the log like below if application has started succesfully**

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

# Docker deployment

**Change following property in `application.properties` file**

`spring.datasource.url=jdbc:postgresql://localhost:5432/postgres`

to

`spring.datasource.url=jdbc:postgresql://postgres:5432/postgres`

**Make sure Postgres is not running**

Go to the project directory and run following commands to build the jar and run the application on Docker

`mvn clean package`

`docker-compose build`
`docker-compose up`

**Should see DB query in the log like below if application has started succesfully**

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

`spring-boot-customer-app_1  | Hibernate: insert into customer (name, customer_id) values (?, ?)`

# API details

`GET` http://localhost:8080/api/customer `Get all customers`

`GET` http://localhost:8080/api/customer/{custimerId} `Get a customer`

`POST` http://localhost:8080/api/customer `Create a new Customer`

**Request**
```JSON
{
    "name": "Jitendra Shukla"
}
```
`PUT` http://localhost:8080/api/customer `Modify an exsting Customer`

**Request**
```JSON
{
    "customerId": 5319886029,
    "name": "Jitendra"
}
```
`DELTE` http://localhost:8080/api/customer/{custimerId} `DELETE a customer`


