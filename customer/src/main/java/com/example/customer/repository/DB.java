package com.example.customer.repository;

import static com.example.customer.util.IdGen.getId;

import java.util.HashSet;
import java.util.Set;

import com.example.customer.model.Customer;;

public class DB {

    private static Set<Customer> customers = new HashSet<>(init());

    private static Set<Customer> init() {

        Customer c1 = new Customer(getId(), "C1");
        Customer c2 = new Customer(getId(), "C2");
        Customer c3 = new Customer(getId(), "C3");
        Customer c4 = new Customer(getId(), "C4");
        Customer c5 = new Customer(getId(), "C5");
        Customer c6 = new Customer(getId(), "C6");
        Customer c7 = new Customer(getId(), "C7");
        
        return Set.of(c1, c2, c3, c4, c5, c6, c7);

    }
    
    public static Set<Customer> getCustomers() {
    	return customers;
    }
}
