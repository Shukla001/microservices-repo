package com.example.customer.resource;

import static com.example.customer.util.IdGen.getId;
import static java.util.Objects.isNull;

import java.util.HashSet;
import java.util.Set;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.customer.excpetion.CustomerNotFoundException;
import com.example.customer.model.Customer;
import com.example.customer.repository.CustomerRepositoty;

@RestController
@RequestMapping("/api/customer")
public class CustomerResource {
	
	CustomerRepositoty repositoty;
	
	public CustomerResource(CustomerRepositoty repo) {
		super();
		this.repositoty = repo;
		
	}

	@PostMapping
	public Customer create(@RequestBody Customer c) {
		
		if(isNull(c)) {
			throw new IllegalArgumentException();
		}
		
		c.setCustomerId(getId());
		repositoty.save(c);
		return c;
		
	}
	
	@PutMapping
	public Customer update(@RequestBody Customer c) {
		
		if(isNull(c) || isNull(c.getCustomerId())) {
			throw new IllegalArgumentException();
		}
		
		Customer customer = repositoty.findById(c.getCustomerId()).orElseThrow(CustomerNotFoundException::new);
		customer.setName(c.getName());
		repositoty.save(customer);
		return customer;
		
	}
	
	@DeleteMapping("/{id}")
	public Customer delete(@PathVariable Long id) {
		
		Customer customer = repositoty.findById(id).orElseThrow(CustomerNotFoundException::new);
		repositoty.delete(customer);
		return customer;
	}

	@GetMapping("/{id}")
	public Customer get(@PathVariable Long id) {
		
		Customer customer = repositoty.findById(id).orElseThrow(CustomerNotFoundException::new);
		return customer;
	}
	
	@GetMapping
	public Set<Customer> getAll() {
		Set<Customer> customers = new HashSet<>();
		repositoty.findAll().iterator().forEachRemaining(customers::add);
		return customers;
	}
	
	@GetMapping("/greeting")
	public String getting() {
		return "Hello World !";
	}
	
}
