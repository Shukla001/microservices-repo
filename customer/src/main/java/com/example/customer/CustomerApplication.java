package com.example.customer;

import static com.example.customer.util.IdGen.getId;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.customer.model.Customer;
import com.example.customer.repository.CustomerRepositoty;

@SpringBootApplication
public class CustomerApplication implements CommandLineRunner{
	
	@Autowired
	CustomerRepositoty repo;

	public static void main(String[] args) {
		SpringApplication.run(CustomerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Customer c1 = new Customer(getId(), "C1");
        Customer c2 = new Customer(getId(), "C2");
        Customer c3 = new Customer(getId(), "C3");
        Customer c4 = new Customer(getId(), "C4");
        Customer c5 = new Customer(getId(), "C5");
        Customer c6 = new Customer(getId(), "C66");
        
        Set<Customer> customers = Set.of(c1, c2, c3, c4, c5, c6);
        
        repo.saveAll(customers);
	}

}
